/**
 * Function to detect mentioning when it is occuring at the end of the text.
 * @function detectLastMention
 * @param {String} text - Text to check for mentioning
 * @returns {Array} - Array of mentioned names
 */
export const detectLastMention = (text) => {
  const mentionRegex = /@[a-zA-Z0-9_]*$/gi
  return text.match(mentionRegex) ?? []
}

/**
 * Function to detect mentioning and return an array of mentioned names inside a text
 * @function detectAllMentions
 * @param {String} text - Text to check for mentioning
 * @returns {Array} - Array of mentioned names
 */
export const detectAllMentions = (text) => {
  const mentionRegex = /@[a-zA-Z0-9_]*/gi
  return text.match(mentionRegex) ?? []
}
