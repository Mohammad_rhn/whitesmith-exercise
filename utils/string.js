/**
 * Function to sanitize string and remove malicious characters
 * @function sanitizeString
 * @param {String} str - Text to sanitize
 * @returns {String} - Sanitzied text
 */
export const sanitizeString = (str) => {
  str = str.replace(/[^@a-z0-9 .,_-]/gim, '')
  return str.trim()
}
