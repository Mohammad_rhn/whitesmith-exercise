import { detectAllMentions } from '@/utils/mentioning'

export const requestNotificationPermission = () => {
  Notification.requestPermission()
}

/**
 * Function which gets notification title and body and triggers a browser notification
 * @function spawnNotification
 * @param {String} title - Title of the notification
 * @param {String} body - Message of the notification
 * @returns {Notification} - The instance of the newly created notification
 */
export const spawnNotification = (title, body) => {
  const options = {
    body,
  }
  return new Notification(title, options)
}

/**
 * Function that gets a message and the current user and if there was any mention of the user, shows a notification
 * @function handleMessageNotification
 * @param {Object} message - The message to check its text fot mentions
 * @param {Object} user - Current user of the messenger
 */
export const handleMessageNotification = (message, user) => {
  const mentions = detectAllMentions(message.text)

  mentions.forEach((mention) => {
    if (mention === `@${user.username}`) {
      spawnNotification(message.user.name, message.text)
    }
  })
}
