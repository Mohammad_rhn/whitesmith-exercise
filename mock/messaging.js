// Imaginary users
export const users = [
  { userId: '2', name: 'John', username: 'John_Doe' },
  { userId: '3', name: 'Jane', username: 'Jane_Foo' },
  { userId: '4', name: 'Bob', username: 'Bob_Bar' },
  { userId: '5', name: 'Jeff', username: 'Jeff_Beth' },
]

/**
 * A mock function to handle mock message creation
 * @function createMockMessage
 * @param {number} messageId - Id of the message to be created
 * @returns {Object} - Created message
 */
export const createMockMessage = (messageId) => {
  // Possible messages
  const messages = [
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean non mi conse',
    'rtor in hendrerit. Mauris vehicula consectetur ligu',
    'Aliquam vestibulum vel purus nec vestibulum. Sed non felis efficitur, eleifend felis eget, laoreet lectus. Aenean accumsan gravida dui placerat mollis. Proin sit amet ris',
    'Pmauris placerat, euismod dignissim nunc pretium. Pellentesque efficitur efficitur felis a consectetur. Vestibulum accumsan mollis tempor.',
    'Morbi elementum, magna ac viverra',
  ]

  return {
    text: messages[Math.floor(Math.random() * messages.length)], // Selects a random message from messages array
    messageId,
    time: new Date(),
    user: users[Math.floor(Math.random() * users.length)], // Selects a random user from users array
  }
}
