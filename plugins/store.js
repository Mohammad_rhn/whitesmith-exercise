import Vue from 'vue'
import { users } from '@/mock/messaging'
import { handleMessageNotification } from '@/utils/notification'

const state = Vue.observable({
  user: {
    // Current user
    userId: '1',
    name: 'Mohammad',
    username: 'Mohammad_Rhn',
  },
  users: [
    { userId: '1', name: 'Mohammad', username: 'Mohammad_Rhn' },
    ...users,
  ],
  messages: [],
})

// Creating a VueX like store object, So we can change the $store to VueX whenever we want without having side effects in vue components.
const store = {
  state,
  mutations: {
    /**
     * Function to add a message to the list of messages
     * @function addMessage
     * @param {Object} payload
     */
    addMessage(payload) {
      const { message } = payload

      // Even if no page or component was mounted we still should show the notification to the user (when system receives a new message),
      // So we are handling mention notification here
      handleMessageNotification(message, state.user)

      state.messages.push(message)
    },
  },
  /**
   * Commit function to call store mutations
   * @function commit
   * @param {String} mutationName - Name of the mutation to be called
   * @param {Object} payload - Data to use as argument
   */
  commit(mutationName, payload) {
    this.mutations[mutationName](payload)
  },
}

export default ({ app }, inject) => {
  inject('store', store)
}
